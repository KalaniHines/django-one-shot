from django.urls import path
from todos.views import (
    todo_list,
    show_list,
    create_list,
)

urlpatterns = [
    path("create/", create_list, name="todo_list_create"),
    path("<int:id>/", show_list, name="todo_list_detail"),
    path("", todo_list, name="todo_list_list"),
]
