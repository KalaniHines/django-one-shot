from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList
from todos.forms import TodoForm

# Create your views here.
def todo_list(request):
    todos = TodoList.objects.all()
    context = {
        "todos": todos,
    }
    return render(request, "todos/list.html", context)


def show_list(request, id):
    todo_detail = get_object_or_404(TodoList, id=id)
    context = {
        "todo_detail": todo_detail,
    }
    return render(request, "todos/detail.html", context)


def create_list(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoForm
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)
